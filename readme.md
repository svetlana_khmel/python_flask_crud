## Learn Python ##
https://www.tutorialspoint.com/python/python_functions.htm
https://www.programiz.com/python-programming/examples

## virtual environment ##
When working with Python projects, it's always a good idea to create a virtual environment. This allows you to create an isolated environment, separate from the system version of Python. Any changes you make to this virtual environment only affects the single project, nothing else. In this way, it's a very safe way to test your projects as they can be deleted and rebuilt very easily. View the following article for further details.

Installing and using virtualenv with Python 3
pip3 install virtualenv
You'll need the full path to the Python 3 version of virtualenv, so run the following to view it:
which virtualenv
/Library/Frameworks/Python.framework/Versions/3.7/bin/virtualenv
Navigate to your site's directory, where you'll create the new virtual environment:
[server]$ cd ~/example.com
Source your .bash_profile
[server]$ . ~/.bash_profile

Create the virtual environment
virtualenv -p /home/example_username/opt/python-3.6.2/bin/python3 my_project

To activate the new virtual environment, run the following:
source my_project/bin/activate

##### Deactivating your virtualenv ####
When finished working in the virtual environment, you can deactivate it by running the following:

[server]$ deactivate
This puts you back into your Shell user's default settings.
Deleting your virtual environment
To delete a virtual environment, simply delete the project folder. Using the previous example, run the following command:

[server]$ rm -rf my_project

###Documentation:###
https://www.programiz.com/python-programming/examples
###Lessons:###
https://www.youtube.com/watch?v=Pu9XhFJduEw&list=PL1FgJUcJJ03vLZXbAFESDqGKBrDNgi-LG
https://github.com/parwiz123/flask-crud-application-with-mysql


!!!!! https://help.dreamhost.com/hc/en-us/articles/115000699011-Using-pip3-to-install-Python3-modules
You can try next time: https://www.python-boilerplate.com/py3+flask+gitignore+logging+unittest

https://www.jetbrains.com/help/pycharm/creating-flask-project.html
https://www.jetbrains.com/help/pycharm/run-debug-configuration-flask-server.html

### mySQL ###
https://dev.mysql.com/doc/mysql-getting-started/en/

Start MySQL
sudo /usr/local/mysql/support-files/mysql.server start

Stop MySQL
sudo /usr/local/mysql/support-files/mysql.server stop

Restart MySQL
sudo /usr/local/mysql/support-files/mysql.server restart

OR :::
system preferences -> mySQL
Once your MySQL server is up and running, you can connect to it as the superuser root with the mysql client.

open -t .bash_profile
 MYSQL_HOME=/usr/local/mysql/bin/mysql 
 export PATH=$MYSQL_HOME/bin:$PATH
 
 

mysql -u root -p
ALTER USER 'root'@'localhost' IDENITIFIED BY 'MyNewPass'
### How To Create a New User and Grant Permissions in MySQL ###
https://www.digitalocean.com/community/tutorials/how-to-create-a-new-user-and-grant-permissions-in-mysql

show databases
use database_name;
show tables;


https://flask-mysqldb.readthedocs.io/en/latest/#flask_mysqldb.MySQL.connection
https://www.youtube.com/watch?v=ltYNt44c2xo

Authentication plugin 'caching_sha2_password' cannot be loaded

Open MySQL from System Preferences > Initialize Database >
Type your new password.
Choose 'Use legacy password'
Start the Server again.
Now connect the MySQL Workbench

you can change the encryption of the password like this.

ALTER USER 'yourusername'@'localhost' IDENTIFIED WITH mysql_native_password BY 'youpassword';

$ echo '.idea' >> .gitignore
$ git rm -r --cached .idea
$ git add .gitignore
$ git commit -m '(some message stating you added .idea to ignored entries)'
$ git push


Django, CherryPy, or Flask?
https://www.netsolutions.com/insights/top-10-python-frameworks-for-web-development-in-2019/

### Install Python 3 on Mac && Virtual Environments ###
https://wsvincent.com/install-python3-mac/

You should always use a virtual environment for your Python projects. A virtual environment is a way to create an isolated space so you can, for example, run Python 2.7 for one project and Python 3.7 for another on the same computer. We can use the built-in venv module for this.

It’s a best practice to keep all your virtualenvs in one place, for example .virtualenvs/ in your home directory. Let’s create that directory:

$ mkdir ~/.virtualenvs
Now create a new virtual environment called myvenv by running:

$ python3 -m venv ~/.virtualenvs/myvenv
Because we used python3 here our virtual environment knows that when we type python for a command we mean Python 3 not Python 2. In order to activate this virtual environment, so we can use it, we must also run the following command:

$ source ~/.virtualenvs/myvenv/bin/activate
(myvenv) $
Note that while the environment is active, you will see its name in parentheses. Software packages we install now will only be available within this virtual environment. You can use the command pip freeze to see all installed software within a virtual environment.

To stop using a virtual environment, either close the Terminal window or enter deactivate:

(myvenv) $ deactivate
$

https://www.youtube.com/watch?v=j2v2r6ByjJI

#### ORM for Python (poor working :()####
https://www.fullstackpython.com/object-relational-mappers-orms.html

Examples 
https://github.com/kiteco/kite-python-blog-post-code
https://www.python-course.eu/sql_python.php

## Trobleshooting ##
python -V
Pip install flask

Run python3
sudo easy_install pip
Pip install Flask

pip install flask-mysqldb
brew install mysql-connector-c
mysql_config (найти его: which mysql_config)

libs="-L$pkglibdir"
libs="$libs -l "

Should be:
libs="-L$pkglibdir"
libs="$libs -lmysqlclient -lssl -lcrypto"
--> PyCharm shows unresolved references error for valid code
56

There are many solutions to this, some more convenient than others, and they don't always work.

Here's all you can try, going from 'quick' to 'annoying':

Do File -> Invalidate Caches / Restart and restart PyCharm.
You could also do this after any of the below methods, just to be sure.
First, check which interpreter you're running: Run -> Edit Configurations -> Configuration -> Python Interpreter.
Refresh the paths of your interpreter:
File -> Settings
Project: [name] -> Project Interpreter -> 'Project Interpreter': Gear icon -> More...
Click the 'Show paths' button (bottom one)
Click the 'Refresh' button (bottom one)
Remove the interpreter and add it again:
File -> Settings
Project: [name] -> Project Interpreter -> 'Project Interpreter': Gear icon -> More...
Click the 'Remove' button
Click the 'Add' button and re-add your interpeter
Delete your project preferences
Delete your project's .idea folder
Close and re-open PyCharm
Open your project from scratch
Delete your PyCharm user preferences (but back them up first).
~/.PyCharm50 on Mac
%homepath%/.PyCharm50 on Windows
Switch to another interpreter, then back again to the one you want.
Create a new virtual environment, and switch to that environments' interpreter.
Switch to another interpreter altogether, don't switch back.


### mySQL cheatsheet ###
http://g2pc1.bu.edu/~qzpeng/manual/MySQL%20Commands.htm

### Working connector ####
https://dev.mysql.com/doc/connector-python/en/connector-python-installation-binary.html

Installing Connector/Python with pip
Use pip to install Connector/Python on most any operating system:


shell> pip install mysql-connector-python

##### WORKING!!!! Inserting Data Using Connector/Python #####
INSERT DATA: https://dev.mysql.com/doc/connector-python/en/connector-python-example-cursor-transaction.html
SELECT DATA: https://dev.mysql.com/doc/connector-python/en/connector-python-example-cursor-select.html
DELETE ROW: https://www.w3schools.com/python/python_mysql_delete.asp

https://www.javatpoint.com/python-mysql-database-connection


Code examples 
https://www.programcreek.com/python/example/93044/mysql.connector.connect

### Deploy Flask app ###
to aws c2
https://www.youtube.com/watch?v=-Gc8CMjQZfc
https://www.youtube.com/watch?v=dBgiMtSqbWQ
https://medium.com/@rodkey/deploying-a-flask-application-on-aws-a72daba6bb80

https://towardsdatascience.com/deploying-a-python-web-app-on-aws-57ed772b2319


### Deploy Flask to Heroky ###
https://medium.com/the-andela-way/deploying-a-python-flask-app-to-heroku-41250bda27d0


# Next steps: #
- Split code to separate files
- Object Orientated example
- Deploy
- How to work with dependencies
- Authorization







