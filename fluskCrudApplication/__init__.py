from flask import Flask, render_template, request, redirect, url_for, flash
from datetime import date, datetime, timedelta
import mysql.connector
from settings import user, password, database
from db_connect import cnx, cursor

app = Flask(__name__)
# Secret key for flash messages
app.secret_key = 'many random bytes'

# cnx = mysql.connector.connect(user=user, database=database, password=password)
# cursor = cnx.cursor()

@app.route('/')
def Index():
    cnx = mysql.connector.connect(user=user, database=database, password=password)
    cursor = cnx.cursor()
    query = "SELECT id, name, email, phone FROM students"
    cursor.execute(query)
    data = cursor.fetchall()

    for (name, email, phone) in cursor:
        print("{}, {} was hired on {}".format(
            name, email, phone))

    cursor.close()
    cnx.close()
    return render_template('index.html', students=data )

@app.route('/insert', methods = ['POST'])
def insert():

    if request.method == "POST":
        flash("Data Inserted Successfully")
        name = request.form['name']
        email = request.form['email']
        phone = request.form['phone']
        add_employee = ("INSERT INTO students "
                        "(name, email, phone) "
                        "VALUES (%s, %s, %s)")

        data_employee = (name, email, phone)

        # Insert new employee
        cursor = cnx.cursor()
        cursor.execute(add_employee, data_employee)
        emp_no = cursor.lastrowid

        # Make sure data is committed to the database
        cnx.commit()

        cursor.close()
        cnx.close()


        return redirect(url_for('Index'))

@app.route('/update',methods=['POST','GET'])
def update():
    if request.method == 'POST':
        flash("Record Has Been Updated Successfully")
        id_data = request.form['id']
        name = request.form['name']
        email = request.form['email']
        phone = request.form['phone']
        cnx = mysql.connector.connect(user=user, database=database, password=password)
        cursor = cnx.cursor()
        query = """
               UPDATE students
               SET name=%s, email=%s, phone=%s
               WHERE id=%s
            """
        cursor.execute(query, (name, email, phone, id_data,))
        cnx.commit()
        cursor.close()
        cnx.close()
        return redirect(url_for('Index'))

@app.route('/delete/<string:id_data>', methods = ['GET'])
def delete(id_data):
    # DELETE from [table name] where [field name] = 'whatever';
    flash("Record Has Been Deleted Successfully")
    query = "DELETE FROM students WHERE id=%s"
    cnx = mysql.connector.connect(user=user, database=database, password=password)
    cursor = cnx.cursor()
    cursor.execute(query, (id_data,))

    # Make sure data is committed to the database
    cnx.commit()

    cursor.close()
    cnx.close()
    print(cursor.rowcount, "record(s) updated")
    return redirect(url_for('Index'))


if __name__ == '__main__':
    app.run(debug=True)
    app.run(host='0.0.0.0', port=4996)
