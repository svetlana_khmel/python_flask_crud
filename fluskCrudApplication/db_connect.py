import mysql.connector
from settings import user, password, database

cnx = mysql.connector.connect(user=user, database=database, password=password)
cursor = cnx.cursor()